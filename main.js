//Теоретичні питання
//Опишіть, як можна створити новий HTML тег на сторінці.
//document.createElement(tag)

//Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі 
//варіанти цього параметра.
//insertAdjacentHTML розбирає вказаний текст як html та вставляє отримані вузли в ДОМ дерево
//в залежності від значення параметру where:
//"beforebegin" – вставляє html прямо перед elem,
//"afterbegin" – вставляє html в elem, на початку,
//"beforeend" – вставляє html в elem, в кінці,
//"afterend" – вставляє html відразу після elem.

//Як можна видалити елемент зі сторінки?
//Щоб видалити елемент HTML потрібно використовувати remove() метод,
 //а для дочірнього вузла removeChild(child)


//Завдання
//Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку 
//у вигляді списку. Завдання має бути виконане на чистому Javascript без використання 
//бібліотек типу jQuery або React.

//Технічні вимоги:
//Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent -
// DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
//кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
//Приклади масивів, які можна виводити на екран:

//["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//["1", "2", "3", "sea", "user", 23];
//Можна взяти будь-який інший масив.

//Необов'язкове завдання підвищеної складності:
//Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив,
// виводити його як вкладений список. Приклад такого масиву:

//["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
//Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди)
// перед очищенням сторінки.

//const ul = document.createElement("ul");
//document.body.append (ul);
//const showMessage = (array, parentElement) =>{ {
//console.log(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], ul)};


(function(){
let ul = document.createElement('ul');
document.body.append(ul);
town = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
town.forEach(renderTownList);
function renderTownList(parantElement, arr) {
let li = document.createElement('li');
ul.appendChild(li);
li.innerHTML=li.innerHTML + parantElement;
console.log (ul);
}
})();